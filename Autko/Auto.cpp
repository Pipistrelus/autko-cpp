#include "Auto.h"

Auto::Auto(const Auto& autko) : paliwo(autko.paliwo), bieg(autko.bieg), predkosc(autko.predkosc) {}

Auto::Auto() {}

Auto::~Auto() {}

Auto Auto::operator=(const Auto& autko) {
	if (this != &autko) {
		this->~Auto();
		this->paliwo = autko.paliwo;
		this->bieg = autko.bieg;
		this->predkosc = autko.predkosc;
	}
	return *this;
}

void Auto::jedz() {}

void Auto::hamuj() {}

void Auto::zmien_bieg() {}

int Auto::getPaliwo() {
	return this->paliwo;
}

int Auto::getBieg() {
	return this->bieg;
}

int Auto::getPredkosc() {
	return this->predkosc;
}

void Auto::setPaliwo(int paliwo) {
	this->paliwo = paliwo;
}

void Auto::setBieg(int bieg) {
	this->bieg = bieg;
}

void Auto::setPredkosc(int predkosc) {
	this->predkosc = predkosc;
}

std::ostream& operator<<(std::ostream& op, const Auto& autko) {
	return op << autko.paliwo << " " << autko.bieg << " " << autko.predkosc;
}

std::istream& operator>>(std::istream& op, Auto& autko) {
	int paliwo, bieg, predkosc;
	op >> paliwo >> bieg >> predkosc;
	autko.setPaliwo(paliwo);
	autko.setBieg(bieg);
	autko.setPredkosc(predkosc);
	return op;
}
