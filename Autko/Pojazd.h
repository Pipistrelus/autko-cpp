#pragma once

class Pojazd {
public:
	virtual void jedz() = 0;
	virtual void hamuj() = 0;
	virtual void zmien_bieg() = 0;
};