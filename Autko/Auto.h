#pragma once
#include <iostream>
#include "Pojazd.h"

class Auto : public Pojazd {
private:
	int paliwo;
	int bieg;
	int predkosc;

public:
	Auto(const Auto& autko);
	Auto();
	~Auto();

	Auto operator=(const Auto& autko);
	friend std::ostream& operator<<(std::ostream& op, const Auto& autko);
	friend std::istream& operator>>(std::istream& op, Auto& autko);

	virtual void jedz() override;
	virtual void hamuj() override;
	virtual void zmien_bieg() override;

	int getPaliwo();
	int getBieg();
	int getPredkosc();

	void setPaliwo(int paliwo);
	void setBieg(int bieg);
	void setPredkosc(int predkosc);
};